(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "trivial-garbage")
  (asdf:load-system "trivial-garbage/tests"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(unless (rtest:do-tests)
  (uiop:quit 1))
